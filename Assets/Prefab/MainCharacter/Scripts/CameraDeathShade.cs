using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
public class CameraDeathShade : MonoBehaviour
{
    public float shadeSpeed = 1f;
    private ColorAdjustments _ca;

    private bool _ended = false;
    public UnityEvent end;

    public void Start(){
        GetComponent<Volume>().profile.TryGet<ColorAdjustments>(out _ca);
    }


    void Update (){
        _ca.colorFilter.value = Vector4.Lerp(_ca.colorFilter.value, new Vector4(0,0,0,1), shadeSpeed*Time.deltaTime); 
        if (_ca.colorFilter.value.grayscale<=0.01&&!_ended){
            end.Invoke();
            _ended = true;
            }
    }
}
