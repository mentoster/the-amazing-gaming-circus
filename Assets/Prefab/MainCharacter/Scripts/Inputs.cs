using System.Net.WebSockets;
using UnityEngine;
#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
#endif

public class Inputs : MonoBehaviour
{
	[Header("Character Input Values")]
	public Vector2 move;
	public Vector2 look;
	public bool jump;
	public bool sprint;
	public bool fire;
	public bool dash;

	[Header("Movement Settings")]
	public bool analogMovement;

	[Header("Mouse Cursor Settings")]
	public bool cursorLocked = false;
	public bool cursorInputForLook = true;
    [SerializeField] private GameObject _mobileControls;
    private bool _isPc = true;
    private void Update()
    {
        // if we press any key  (except mouse) on the keyboard, we hide the mobile controls
        if (Input.anyKeyDown && !Input.GetMouseButtonDown(0) && !Input.GetMouseButtonDown(1) && !Input.GetMouseButtonDown(2))
        {
            _mobileControls.SetActive(false);
            _isPc = true;
        }
        if (_isPc)
        {
            SetCursorLocked(true);
            cursorLocked = true;
            cursorInputForLook = true;
        }
    }
#if ENABLE_INPUT_SYSTEM
    public void OnMove(InputValue value)
	{
		MoveInput(value.Get<Vector2>());
	}

	public void OnLook(InputValue value)
	{
		if (cursorInputForLook)
		{
			LookInput(value.Get<Vector2>());
		}
	}

	public void OnJump(InputValue value)
	{
		JumpInput(value.isPressed);
	}

	public void OnSprint(InputValue value)
	{
		SprintInput(value.isPressed);
	}

	public void OnFire(InputValue value)
	{
		FireInput(value.isPressed);
	}

	public void OnDash(InputValue value)
	{
		DashInput(value.isPressed);
	}

#endif

	public void MoveInput(Vector2 newMoveDirection)
	{
		move = newMoveDirection;
	}

	public void LookInput(Vector2 newLookDirection)
	{
		look = newLookDirection;
	}

	public void JumpInput(bool newJumpState)
	{
		jump = newJumpState;
	}

	public void SprintInput(bool newSprintState)
	{
		sprint = newSprintState;
	}

	public void FireInput(bool newFireState)
	{
		fire = newFireState;
	}

	public void DashInput(bool newDashState)
	{
		dash = newDashState;
	}


	private void OnApplicationFocus(bool hasFocus)
	{
		SetCursorState(cursorLocked);
	}

	public void SetCursorState(bool newState)
	{
		Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
	}

	public void SetCursorLocked(bool value)
	{
		cursorLocked = value;

		SetCursorState(cursorLocked);
	}
}
