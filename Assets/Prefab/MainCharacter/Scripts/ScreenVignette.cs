using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ScreenVignette : MonoBehaviour
{
    public float maxVignette = 1f;
    public float maxChromaticAberration = 1f;

    private Volume _volume;

    public void Start(){
        _volume = GetComponent<Volume>();
        SetValue(1);
    }

    public void SetValue(float value){
        value = 1 - Mathf.Clamp01(value);
        Vignette v;
        _volume.profile.TryGet<Vignette>(out v);
        v.intensity.value = value*maxVignette; 

        ChromaticAberration ca;
        _volume.profile.TryGet<ChromaticAberration>(out ca);
        ca.intensity.value = value*maxChromaticAberration;
    }


}
