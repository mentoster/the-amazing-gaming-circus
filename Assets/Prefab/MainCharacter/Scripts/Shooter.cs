using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class Shooter : MonoBehaviour
{
    public Object projectile;
    public float shootForce = 1f;
    public float maxOverheat = 10f;
    public float coolingDelay = 1f;
    public float shootDelay = 0.5f;
    public float heatPerShoot = 1f;
    public EventReference eventReference;
    private FMOD.Studio.EventInstance _badWordsInstance;


    [Tooltip("Where the projectile should spawn")]
    public Transform origin;
    [Tooltip("On forwrard direction of eye the projectile will shoot")]
    public Transform eye;
    public Vector3 originOffset = Vector3.zero;

    private float _coolingTimeout = 0;
    private float _shootTimeout = 0;
    private float _currentOverheat = 0;

    private Inputs _input;
    private ThirdPersonController _tpc;

    public UnityEvent<string> shooted;
    public UnityEvent<float> overheatChanged;


    private void Start()
    {
        _input = GetComponent<Inputs>();
        _tpc = GetComponent<ThirdPersonController>();
    }

    public void Update()
    {
        Shoot();
    }
    public void Shoot()
    {
        _coolingTimeout -= _coolingTimeout > 0 ? Time.deltaTime : 0;
        _shootTimeout -= _shootTimeout > 0 ? Time.deltaTime : 0;
        _currentOverheat -= _currentOverheat > 0 && _coolingTimeout <= 0 ? Time.deltaTime : 0;

        overheatChanged.Invoke(_currentOverheat / maxOverheat);
        if (_shootTimeout <= 0 && _currentOverheat <= maxOverheat && _input.fire)
        {
            _input.fire = false;

            _shootTimeout = shootDelay;
            _currentOverheat += heatPerShoot;

            Vector3 direction = eye.forward - Vector3.up * eye.forward.y;

            GameObject newProjectile = Instantiate(projectile, Quaternion.LookRotation(direction, Vector3.up) * originOffset + origin.position, Random.rotation) as GameObject;
            newProjectile.tag = gameObject.tag;

            newProjectile.GetComponent<Rigidbody>().AddForce(direction * shootForce, ForceMode.Impulse);

            shooted.Invoke("Fire");
            PlaySound();

            transform.rotation = Quaternion.AngleAxis(Vector3.SignedAngle(Vector3.forward, direction, Vector3.up), Vector3.up);
        }
    }
    public void PlaySound()
    {
        // Create the instance
        _badWordsInstance = RuntimeManager.CreateInstance(eventReference);

        // Set the 3D attributes once upon creation, if the position is not supposed to change
        RuntimeManager.AttachInstanceToGameObject(_badWordsInstance, transform, GetComponent<Rigidbody>());

        // Start the event instance
        _badWordsInstance.start();
    }

}
