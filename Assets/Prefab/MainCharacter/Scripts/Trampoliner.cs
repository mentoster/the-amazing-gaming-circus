using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoliner : MonoBehaviour
{
    public float jumpHeight = 5f;
    public string trampolineTag = "trampoline"; 
    private ThirdPersonController _tpr;
    
    private void Start(){
        _tpr = GetComponent<ThirdPersonController>();
    }
    
    public void OnTriggerEnter(Collider collider){
        if (collider.gameObject.tag == trampolineTag){
            _tpr.Jump(jumpHeight);
        }
    }
}
