using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Holder : MonoBehaviour
{
    public void OnTriggerEnter(Collider collider){
        CharacterController cc = collider.gameObject.GetComponent<CharacterController>();
        if (!cc)
            return;
        cc.transform.SetParent(transform);
    }
    public void OnTriggerExit(Collider collider){
        CharacterController cc = collider.gameObject.GetComponent<CharacterController>();
        if (!cc)
            return;
        cc.transform.SetParent(null);
    }
}
