using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class ImageDVD : MonoBehaviour
{
    public RectTransform rectTransform;
    public float speed;

    private float _screenWidth;
    private float _screenHeight;
    private float _imageWidth;
    private float _imageHeight;
    private Vector2 _direction;

    void Start()
    {
        _screenWidth = Screen.width;
        _screenHeight = Screen.height;
        _imageWidth = rectTransform.rect.width;
        _imageHeight = rectTransform.rect.height;
        _direction = new Vector2(1, 1).normalized;

        rectTransform.localPosition = new Vector3(Random.Range((-_screenWidth + _imageWidth)/2, (_screenWidth - _imageWidth)/2 ), 
        Random.Range( (-_screenHeight+ _imageHeight) / 2, (_screenHeight-_imageHeight) / 2), 0);
    }

    void Update()
    {
        Vector3 currentPosition = rectTransform.localPosition;

        if (currentPosition.x + _imageWidth / 2 >= _screenWidth / 2 || currentPosition.x - _imageWidth / 2 <= -_screenWidth / 2)
        {
            _direction.x *= -1;
        }

        if (currentPosition.y + _imageHeight / 2 >= _screenHeight / 2 || currentPosition.y - _imageHeight / 2 <= -_screenHeight / 2)
        {
            _direction.y *= -1;
        }

        currentPosition += new Vector3(_direction.x * speed * Time.deltaTime, _direction.y * speed * Time.deltaTime, 0);
        rectTransform.localPosition = currentPosition;

        
    }
}
