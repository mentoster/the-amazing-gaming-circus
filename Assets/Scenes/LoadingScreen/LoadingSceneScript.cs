using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingSceneScript : MonoBehaviour
{
    public string nextScene = "Circus";
    public float timeToLoad = 5.0f;
    private float _timedOut = 0.0f;  
    
    void Start()
    {
        _timedOut = timeToLoad;
    }

    void Update()
    {
        _timedOut -= Time.deltaTime;
        if (_timedOut<=0){
            SceneManager.LoadScene(nextScene);
        }
    }
}
