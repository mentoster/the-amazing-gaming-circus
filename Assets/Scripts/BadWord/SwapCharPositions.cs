using UnityEngine;

public class RandomSwapPositions : MonoBehaviour
{
    private Transform[] _objectsToSwap;

    private void Start()
    {
        // Initialize and store all children objects
        _objectsToSwap = new Transform[transform.childCount];
        for (var i = 0; i < transform.childCount; i++)
        {
            _objectsToSwap[i] = transform.GetChild(i);
        }
        SwapPositions();
    }

    private void SwapPositions()
    {
        // Shuffle the array
        for (var i = 0; i < _objectsToSwap.Length; i++)
        {
            var temp = _objectsToSwap[i];
            var randomIndex = Random.Range(0, _objectsToSwap.Length);
            _objectsToSwap[i] = _objectsToSwap[randomIndex];
            _objectsToSwap[randomIndex] = temp;
        }

        // Assign new positions
        var newPositions = new Vector3[_objectsToSwap.Length];
        for (var i = 0; i < _objectsToSwap.Length; i++)
        {
            newPositions[i] = _objectsToSwap[i].transform.position;
        }

        for (var i = 0; i < _objectsToSwap.Length; i++)
        {
            _objectsToSwap[i].transform.position = newPositions[(i + 1) % _objectsToSwap.Length];
        }
    }
}
