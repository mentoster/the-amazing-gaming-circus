using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BannerSpawner : MonoBehaviour

{
    [SerializeField] private GameObject banner;
    [SerializeField] private float distanceToCamera;
    private GameObject instance;
    [SerializeField] private float bannerLifetime = 10f;
    private Transform cameraTransform;
    void Start()
    {
        cameraTransform = Camera.main.gameObject.transform;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.X)) {
            SpawnBanner();
        }
    }
    public void SpawnBanner()
    {
        if (instance != null)
        {
            Destroy(instance);
        }
        instance = Instantiate(banner, cameraTransform.position, cameraTransform.rotation);
        instance.transform.position += instance.transform.forward * distanceToCamera;
        Destroy(instance, bannerLifetime);
    }
}
