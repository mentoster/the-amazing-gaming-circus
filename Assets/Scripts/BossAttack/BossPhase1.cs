using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(requiredComponent: typeof(LightningAttack))]
public class BossPhase1 : MonoBehaviour
{
    [SerializeField] private List<GameObject> _phase1Platforms;
    [SerializeField] private Material _glitch; // List of materials to apply
    [SerializeField] private List<GameObject> _spawners;
    [SerializeField] private float _changeMaterialInterval = 5f; // Time interval for material change
    [SerializeField] private Animator _brainAnimator;
    [SerializeField] private GameObject _phase2;

    public GameObject lastPlatform;
    private MeshRenderer _lastMeshRenderer;
    private Material _lastMaterial;
    private float _minusY = 18;
    [SerializeField] private float _waitTime = 4;
    private LightningAttack _lightningAttack;

    private void Start()
    {
        StartPhase1();
        _lightningAttack = GetComponent<LightningAttack>();
        _lightningAttack.enabled = false;
    }

    private void StartPhase1()
    {
        foreach (var platform in _phase1Platforms)
        {
            platform.transform.position = new Vector3(platform.transform.position.x, platform.transform.position.y - _minusY, platform.transform.position.z);
        }
        StartCoroutine(Phase1());
        StartCoroutine(RandomMaterialChangeCoroutine());
    }

    IEnumerator Phase1()
    {
        yield return new WaitForSeconds(5);
        _lightningAttack.enabled = true;
        StartCoroutine(EnableSpawners());
        foreach (var platform in _phase1Platforms)
        {
            platform.transform.DOMoveY(platform.transform.position.y + _minusY, 5);
            yield return new WaitForSeconds(_waitTime);
        }
    }
    private IEnumerator EnableSpawners()
    {
        foreach (var spawner in _spawners)
        {
            spawner.gameObject.SetActive(true);
            yield return new WaitForSeconds(10);
        }
    }

    private IEnumerator RandomMaterialChangeCoroutine()
    {
        while (true) // Loop indefinitely
        {
            yield return new WaitForSeconds(_changeMaterialInterval);
            ChangeRandomPlatformMaterial();
        }
    }

    private void ChangeRandomPlatformMaterial()
    {
        if (_lastMeshRenderer != null)
        {
            _lastMeshRenderer.material = _lastMaterial;
            _lastMeshRenderer.GetComponent<MeshCollider>().enabled = true;
        }

        if (_phase1Platforms.Count > 0)
        {
            var randomPlatform = _phase1Platforms[Random.Range(0, _phase1Platforms.Count)];
            lastPlatform = randomPlatform;
            // disable mesh collider
            randomPlatform.GetComponent<MeshCollider>().enabled = false;
            var renderer = randomPlatform.GetComponent<MeshRenderer>();
            _lastMaterial = renderer.material;
            _lastMeshRenderer = renderer;

            renderer.material = _glitch;
        }
    }
    public void BossGetHit()
    {
        _brainAnimator.SetTrigger(name: "FlyToCenter");
        foreach (var spawner in _spawners)
        {
            spawner.gameObject.SetActive(value: false);
        }
        // boss get hit, next phase
        foreach (var platform in _phase1Platforms)
        {
            platform.transform.DOMoveY(platform.transform.position.y - _minusY, 3);
        }
        _phase2.SetActive(true);
        gameObject.SetActive(false);
    }
}
