using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class BossPhase2 : MonoBehaviour
{
    [SerializeField] private LaserAttack _laserAttack;
    [SerializeField] private MovingWave _movingWave;
    [SerializeField] private GameObject _bossHitCollider;
    [SerializeField] private Animator _brainAnimator;

    private void Start()
    {
        _laserAttack.enabled = false;
        StartCoroutine(StartAttack());
    }

    private IEnumerator StartAttack()
    {
        yield return new WaitForSeconds(7f);
        _laserAttack.enabled = true;
        _bossHitCollider.SetActive(true);
    }
    public void BossGetHit()
    {
        Debug.Log("BossGetHit");

        _laserAttack.enabled = false;
        _movingWave.enabled = false;
        _brainAnimator.SetTrigger("Die");
        StartCoroutine(TestCoroutine());
    }
    IEnumerator TestCoroutine()
    {
        Destroy(_brainAnimator.gameObject, 3f);
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene("EndScene");
    }
}
