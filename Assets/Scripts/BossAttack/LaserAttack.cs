using UnityEngine;
using System.Collections;

public class LaserAttack : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public Transform player;
    public Transform shootOrigin;
    public float range = 100f;
    public Color warningColor = Color.yellow;
    public Color attackColor = Color.red;
    public float warningDuration = 5f;
    public float shootInterval = 10f;
    public float attackDuration = 5f;
    [SerializeField] private Health _health;
    private void Start()
    {
        StartCoroutine(AttackSequence());

    }

    private IEnumerator AttackSequence()
    {
        while (true)
        {
            yield return StartCoroutine(WarningPhase());
            yield return StartCoroutine(AttackPhase());
            yield return new WaitForSeconds(shootInterval - warningDuration - attackDuration);
        }
    }

    private IEnumerator WarningPhase()
    {
        var endTime = Time.time + warningDuration;
        var flickerInterval = 0.1f;
        var flickerDecrement = 0.9f;

        lineRenderer.startColor = warningColor;
        lineRenderer.endColor = warningColor;

        while (Time.time < endTime)
        {
            lineRenderer.enabled = !lineRenderer.enabled;
            UpdateLaserPosition(player.position, warningColor); // Update laser position even when flickering
            yield return new WaitForSeconds(flickerInterval);
            flickerInterval *= flickerDecrement;
            flickerInterval = Mathf.Max(flickerInterval, 0.05f);
        }

        lineRenderer.enabled = true;
    }

    private IEnumerator AttackPhase()
    {
        UpdateLaserPosition(player.position, attackColor, true);
        lineRenderer.enabled = true;

        yield return new WaitForSeconds(attackDuration);

        ResetLine();
    }

    private void UpdateLaserPosition(Vector3 targetPosition, Color laserColor, bool isAttacking = false)
    {
        var directionToTarget = (targetPosition - shootOrigin.position).normalized;
        RaycastHit hit;

        if (Physics.Raycast(shootOrigin.position, directionToTarget, out hit, range))
        {
            lineRenderer.SetPositions(new Vector3[] { shootOrigin.position, hit.point });
            if (isAttacking)
            {
                if (hit.collider.transform.CompareTag("Player"))
                {
                    // Call a method to decrease health
                    _health.ChangeHP(-50); // Assuming the Health component has a TakeDamage method
                }
            }
        }
        else
        {
            lineRenderer.SetPositions(new Vector3[] { shootOrigin.position, shootOrigin.position + (directionToTarget * range) });
        }

        lineRenderer.startColor = laserColor;
        lineRenderer.endColor = laserColor;
    }

    private void ResetLine()
    {
        lineRenderer.enabled = false;
    }
}
