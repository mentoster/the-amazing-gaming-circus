using System.Collections;
using UnityEngine;

public class LightningAttack : MonoBehaviour
{
    private Transform _playerTransform; // Assign the player's transform here
    [SerializeField] private GameObject _firstObjectPrefab; // Assign the first prefab to instantiate here
    [SerializeField] private GameObject _secondObjectPrefab; // Assign the second prefab to instantiate here
    [SerializeField] private float _timeToAttack = 3f;
    [SerializeField] private float _timeToDisappear = 3f;

    private GameObject _firstObjectInstance;
    private void Start()
    {
        _playerTransform = GameObject.FindWithTag("Player").transform;
        SpawnObjectsAtPlayerPosition();
    }
    // Use this method to start the spawning coroutine
    public void SpawnObjectsAtPlayerPosition()
    {
        StartCoroutine(SpawnWithDelay());
    }


    private IEnumerator SpawnWithDelay()
    {
        // wait for 5 seconds
        yield return new WaitForSeconds(3f);
        print("Spawning objects in 5 seconds...");
        // Instantiate the first object at the player's position
        _firstObjectInstance = Instantiate(_firstObjectPrefab, _playerTransform.position, Quaternion.identity);
        var LightingCollider = _firstObjectInstance.GetComponent<CapsuleCollider>();
        // y - 1
        _firstObjectInstance.transform.position = new Vector3(_firstObjectInstance.transform.position.x, _firstObjectInstance.transform.position.y, _firstObjectInstance.transform.position.z);
        // Start flickering coroutine
        StartCoroutine(FlickerObject(_firstObjectInstance, _timeToAttack));

        yield return new WaitForSeconds(_timeToAttack);


        var secondObj = Instantiate(_secondObjectPrefab, _firstObjectInstance.transform.position, Quaternion.identity);

        // copy material
        _firstObjectInstance.GetComponent<MeshRenderer>().material = secondObj.GetComponent<MeshRenderer>().material;

        LightingCollider.enabled = true;

        Destroy(_firstObjectInstance, _timeToDisappear);
        Destroy(secondObj, _timeToDisappear);
        SpawnObjectsAtPlayerPosition();

    }
    private IEnumerator FlickerObject(GameObject obj, float duration)
    {
        var endTime = Time.time + duration;
        var maxInterval = 0.5f; // Max interval at the start
        var minInterval = 0.05f; // Min interval by the end

        // Loop for the duration of the flicker effect
        while (Time.time < endTime)
        {
            var remainingTime = endTime - Time.time;
            // Calculate the interval based on the remaining time
            var intervalTime = Mathf.Lerp(minInterval, maxInterval, remainingTime / duration);

            // Flicker the object
            obj.SetActive(!obj.activeSelf);

            // Wait for the calculated interval time
            yield return new WaitForSeconds(intervalTime);
        }

        // Ensure the object is active after flickering ends
        if (!obj.activeSelf)
        {
            obj.SetActive(true);
        }
    }


}
