using UnityEngine;
using DG.Tweening;
using System.Collections; // Make sure to include the DOTween namespace

public class MovingWave : MonoBehaviour
{
    [SerializeField] private GameObject _wavePrefab; // Assign the wave prefab in the Inspector
    [SerializeField] private Vector2 _spawnIntervalDiapason; // Time between each spawn
    [SerializeField] private float _waveDuration = 7f;
    // Start is called before the first frame update
    private void Start()
    {
        // Start the spawning coroutine
        StartCoroutine(SpawnWaves());
    }

    private IEnumerator SpawnWaves()
    {
        // Check if the wave prefab is assigned
        if (_wavePrefab == null)
        {
            Debug.LogError("Wave prefab is not assigned!");
            yield break; // Exit the coroutine if the prefab is not assigned
        }

        // Infinite loop to keep spawning waves
        while (true)
        {
            // Spawn the wave at the specified position with the rotation facing up
            var waveInstance = Instantiate(_wavePrefab, new Vector3(4f, 2.5f, -44f), Quaternion.Euler(90f, 0f, 0f));

            // Move the wave to z = 40 with DOTween
            waveInstance.transform.DOMoveZ(40f, duration: _waveDuration).OnComplete(() =>
                // Delete the wave after the move is complete
                Destroy(waveInstance));

            // Wait for the specified interval before spawning the next wave
            yield return new WaitForSeconds(Random.Range(_spawnIntervalDiapason.x, _spawnIntervalDiapason.y));
        }
    }
}
