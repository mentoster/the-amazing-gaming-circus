using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeHealthOnCollide : MonoBehaviour
{
    [SerializeField] private bool _isWave = false;
    private void OnTriggerEnter(Collider other)
    {
        // log name
        if (other.CompareTag("Player"))
        {
            if (_isWave)
            {
                other.gameObject.transform.parent.GetComponent<Health>().ChangeHP(-30);
                Destroy(gameObject);
            }
            else
            {
                other.gameObject.transform.parent.GetComponent<Health>().ChangeHP(-70);
            }

            GetComponent<CapsuleCollider>().isTrigger = false;
        }
        else if (other.gameObject.name == "BossTrigger" && !_isWave)
        {
            FindObjectOfType<BossPhase1>().BossGetHit();
        }
    }
}
