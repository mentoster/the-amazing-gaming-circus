using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CageLock : MonoBehaviour
{
    [SerializeField] GameObject cage;
    void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(CoinIncreaseButton.coinsCount >= 100)
        {
            CoinIncreaseButton.coinsCount-=100;
            Destroy(cage);
        }
    }
}
