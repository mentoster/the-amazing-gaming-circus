using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Clock : MonoBehaviour
{
    [SerializeField] private TMP_Text _tmpText; // Assign your TextMeshPro text in the inspector


    private void Start()
    {
        if (_tmpText == null)
        {
            Debug.LogError("TMP_Text component not assigned.");
            return;
        }

        // Start the countdown coroutine
        StartCoroutine(CountdownTimer());
    }

    IEnumerator CountdownTimer()
    {
        // Start from 23:59
        int minutes = 59;
        int hours = 23;

        while (hours >= 0)
        {
            // Update the text display each second
            _tmpText.text = string.Format("{0:D2}:{1:D2}", hours, minutes);

            // Wait for one second (simulated hour)
            yield return new WaitForSeconds(1f);

            // Decrement the minutes and check if an hour has passed
            if (--minutes < 0)
            {
                minutes = 59;
                hours--;
            }
        }

        // Optionally, do something when the countdown reaches 00:00
        OnCountdownFinished();
    }

    void OnCountdownFinished()
    {
        // Implement what happens when the countdown finishes
        Debug.Log("Countdown finished!");
    }
}
