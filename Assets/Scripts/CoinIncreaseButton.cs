using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinIncreaseButton : MonoBehaviour
{
    public static int coinsCount;
    [SerializeField] private float delay;
    [SerializeField] private string textPrefix = "";
    [SerializeField] private TMP_Text text;
    [SerializeField] private float pressedOffset;
    private bool canGetCoins = true;
    private Vector3 normalPosition;


    private void Start()
    {
        text.text = textPrefix + coinsCount.ToString();
        normalPosition = transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        transform.position = normalPosition + Vector3.down * pressedOffset;
    }
    private void OnTriggerStay(Collider other)
    {
        if (canGetCoins)
        {
            coinsCount++;
            text.text = textPrefix + coinsCount.ToString();
            canGetCoins = false;
            StartCoroutine(Delay());
        }

    }

    private void OnTriggerExit(Collider other)
    {
        transform.position = normalPosition;
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(delay);
        canGetCoins = true;

    }
}
