using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOnNotRender : MonoBehaviour
{
    public float distanceFromCamera = 10f; 

    private bool _isVisible = true;

    void Update(){
        if (!_isVisible && (Camera.main.transform.position-transform.position).magnitude < 10f)
            gameObject.SetActive(false);
    }

    public void Respawn()
    {
        gameObject.SetActive(true);
    }

    public void SetVisible(bool value){
        _isVisible = value;
    }
    
}
