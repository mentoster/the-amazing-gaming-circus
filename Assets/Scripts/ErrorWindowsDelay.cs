using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErrorWindowsDelay : MonoBehaviour
{
    [SerializeField] private List<GameObject> _objectsToEnable; // Assign this list in the inspector or from another script
    [SerializeField] private float _delayBetweenEach = 0.1f; // Delay of 0.1 seconds

    private void Start()
    {
        EnableObjectsSequentially();
    }
    public void EnableObjectsSequentially()
    {
        StartCoroutine(EnableObjectsCoroutine());
    }

    private IEnumerator EnableObjectsCoroutine()
    {
        // Loop through each GameObject in the reversed list
        _objectsToEnable.Reverse();
        foreach (var obj in _objectsToEnable)
        {
            if (obj != null)
            {
                obj.SetActive(true); // Enable the GameObject
                yield return new WaitForSeconds(_delayBetweenEach); // Wait for 0.1 seconds
            }
        }
    }
}
