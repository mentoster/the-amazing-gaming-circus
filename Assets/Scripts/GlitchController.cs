using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GlitchController : MonoBehaviour
{
    public Color defaultMainColor = Color.white;
    public Color defaultGlitchColor = Color.blue;
    public float defaultGlitchArea = 0f;
    public float defaultdeformation = 0.01f;

    private List<Material> _materials = new List<Material>();
    private void Start(){
        if (GetComponent<MeshRenderer>())
            GetComponent<MeshRenderer>().GetMaterials(_materials);
        foreach ( MeshRenderer mr in GetComponentsInChildren<MeshRenderer>())
            foreach (Material mat in mr.materials)
                _materials.Add(mat);    

        foreach ( SkinnedMeshRenderer smr in GetComponentsInChildren<SkinnedMeshRenderer>())
            foreach (Material mat in smr.materials)
                _materials.Add(mat);  
            
        SetDefaultGlitch();
    }

    public void SetGlitchOn(bool value){
        foreach (Material mat in _materials)
            mat.SetInt("_isOn", value?1:0);
    }

    public void SetMainColor(Color color){
        foreach (Material mat in _materials)
            mat.SetColor("_mainTextureColor", color);
    }
    public void SetGlitchColor(Color color){
        foreach (Material mat in _materials)
            mat.SetColor("_glitchColor", color);
    }

    public void SetGlitchArea(float value){
        foreach (Material mat in _materials)
        {
            mat.SetFloat("_glitchArea", value);
        }
    }
    public void SetDeformation(float value){
        foreach (Material mat in _materials)
            mat.SetFloat("_deformationForce", value);
    }

    public void SetDefaultGlitch(){
        SetMainColor(defaultMainColor);
        SetGlitchColor(defaultGlitchColor);
        SetGlitchArea(defaultGlitchArea);
        SetDeformation(defaultdeformation);
    }
}
