using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GlitchObject : MonoBehaviour
{
    public Collider mainCollider;
    public Color ghostMainColor = new Color(1f,1f,1f,0.5f); 
    public Color ghostGlitchColor = new Color(0f,0f,1f,0.5f); 
    private bool _ghost; 

    private GlitchController _gc;

    public void Start(){
        _gc = GetComponent<GlitchController>();
        SetGhost(false);
    }

    public void OnTriggerEnter(Collider other){
        Health h = other.gameObject.GetComponent<Health>();
        if (!h)
            return;
        if (h.immortal && h.glitchOnImmortal)
            SetGhost(true);
        else
            SetGhost(false);
    }

    public void OnTriggerExit(Collider other){
        Health h = other.gameObject.GetComponent<Health>();
        if (!h)
            return;
        SetGhost(false);
    }

    private void SetGhost(bool value){
        if (value)
        {
            mainCollider.isTrigger = true;
            _gc.SetMainColor(ghostMainColor);
            _gc.SetGlitchColor(ghostGlitchColor);
        }
        else
        { 
            mainCollider.isTrigger = false;
            _gc.SetDefaultGlitch();
        }
    }
}
