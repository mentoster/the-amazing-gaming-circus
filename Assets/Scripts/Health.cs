using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public bool isAbleToGlitch = false;
    public bool immortal = false;
    public float maxHP = 100;

    public bool glitchOnImmortal = false;

    public float regenerationDelay = 5f;
    public float regenerationPerSecond = 20f;
    public float immortalityDelay = 1f;

    [SerializeField]
    private float _currentHP;
    private float _regenerationTimout = 0;
    private float _immortalityTimout = 0;


    public Color glitchColor = Color.red;
    public float glitchDeformation = 0.3f;

    private GlitchController _gc;

    public UnityEvent<string, bool> onDeath;
    public UnityEvent<float> hpChanged;
    public UnityEvent<string> hitted;

    private void Start()
    {
        _gc = GetComponent<GlitchController>();
        _currentHP = maxHP;
        SyncGlitch();
    }

    public void Update()
    {
        _regenerationTimout -= _regenerationTimout > 0 ? Time.deltaTime : 0;
        if (_currentHP < maxHP && _regenerationTimout <= 0)
        {
            ChangeHP(regenerationPerSecond * Time.deltaTime);
        }
        if (_immortalityTimout * (_immortalityTimout - Time.deltaTime) <= 0)
            SetImmortal(false);
        _immortalityTimout -= _immortalityTimout > 0 ? Time.deltaTime : 0;

    }

    public float GetHP()
    {
        return _currentHP;
    }

    public void SetHP(float value)
    {
        _currentHP = value;
        _currentHP = Mathf.Clamp(_currentHP, 0, maxHP);
        hpChanged.Invoke(_currentHP / maxHP);
        SyncGlitch();
        DeathCheck();
    }

    public void ChangeHP(float value)
    {
        if (immortal)
            return;
        _currentHP += value;
        _currentHP = Mathf.Clamp(_currentHP, 0, maxHP);
        if (value < 0)
        {
            hitted.Invoke("Hit");
            _regenerationTimout = regenerationDelay;
        }
        hpChanged.Invoke(_currentHP / maxHP);
        SyncGlitch();
        DeathCheck();
    }

    private void SyncGlitch()
    {
        if (!isAbleToGlitch)
            return;
        if (immortal)
        {
            if (glitchOnImmortal)
                _gc.SetDefaultGlitch();
        }
        else
        {
            if (_regenerationTimout > 0)
                _gc.SetDeformation(glitchDeformation);
            else
                _gc.SetDeformation(_gc.defaultdeformation);
            _gc.SetGlitchColor(glitchColor);
            _gc.SetGlitchArea(1 - _currentHP / maxHP);
        }
    }

    private void DeathCheck()
    {
        if (_currentHP <= 0)
        {
            onDeath.Invoke("Dead", true);
        }
    }

    public void SetImmortal(bool value)
    {
        immortal = value;
        if (immortal)
            _immortalityTimout = immortalityDelay;
        SyncGlitch();
    }

    //костыль, сорян
    public void SetImmortal(string s, bool value)
    {
        if (value)
            SetImmortal(value);
    }

    public void SetGlitchOnImmortal (bool value){
        glitchOnImmortal = value;
    }
}
