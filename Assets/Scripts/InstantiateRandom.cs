using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateRandom : MonoBehaviour
{
    public List<Object> objects = new List<Object>();
    void Start()
    {
        Instantiate(objects[Random.Range(0, objects.Count)], transform.position, transform.rotation, transform.parent);
        Destroy(gameObject);
    }

}
