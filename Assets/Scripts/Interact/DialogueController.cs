using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class DialogueController : MonoBehaviour
{
    public TMP_Text characterText;
    public TMP_Text mainText;

    public UnityEvent OnDisable;

    void Update(){
        if (Input.GetKeyDown(KeyCode.F)){
            OnDisable.Invoke();
            gameObject.SetActive(false);
        }
    }

    public void SetCharacterText(string text){
        characterText.text = text;
    }
    public void SetMainText(string text){
        mainText.text = text;
    }
}