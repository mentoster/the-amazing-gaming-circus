using UnityEngine;

public interface IInteractable
{
    public void Interact();
    string GetInteractText();

    Transform GetTransform();
}
