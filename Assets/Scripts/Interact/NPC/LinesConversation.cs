using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;
using UnityEngine.Events;

public class LinesConversation : MonoBehaviour, IInteractable
{
    public DialogueController dialogueController;
    public string interactText;

    public string nameCharacter;

    public List<string> lines = new List<string>();

    public UnityEvent onConversationStart;
    public UnityEvent onConversationNext;
    public EventReference[] eventReferences;
    private FMOD.Studio.EventInstance _eventInstance;
    private int _index;
    string IInteractable.GetInteractText()
    {
        return interactText;
    }

    Transform IInteractable.GetTransform()
    {
        return transform;
    }

    public void Interact()
    {
        if (lines.Count == 0)
            return;

        GetComponent<BoxCollider>().enabled = false;
        dialogueController.gameObject.SetActive(true);
        dialogueController.SetCharacterText(nameCharacter);
        dialogueController.SetMainText(lines[0]);
        if (lines.Count > 1)
            lines.RemoveAt(0);

        dialogueController.OnDisable.AddListener(() =>
        {
            if (lines.Count > 0)
                GetComponent<BoxCollider>().enabled = true;
            onConversationNext.Invoke();
        });

        PlaySound(eventReferences[_index]);

        if (lines.Count == 0)
        {
            GetComponent<BoxCollider>().enabled = false;
        }
        else
        {
            _index++;
        }
        onConversationStart.Invoke();
    }
    public void PlaySound(EventReference eventReference)
    {
        // Create the instance
        _eventInstance = RuntimeManager.CreateInstance(eventReference);

        // Set the 3D attributes once upon creation, if the position is not supposed to change
        RuntimeManager.AttachInstanceToGameObject(_eventInstance, transform, GetComponent<Rigidbody>());

        // Start the event instance
        _eventInstance.start();
    }
}
