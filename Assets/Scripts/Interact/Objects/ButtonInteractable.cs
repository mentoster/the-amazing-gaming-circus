using UnityEngine;

public class ButtonInteractable : MonoBehaviour, IInteractable
{
    [SerializeField] private string interactText;
    private MeshRenderer _sphereMeshRenderer;
    [SerializeField] private Material _onMaterial;
    [SerializeField] private Material _offMaterial;

    private bool _isEnabled;

    private void Start(){
        _sphereMeshRenderer = GetComponent<MeshRenderer>();
    }

    private void SetEnableMaterial(){
        _sphereMeshRenderer.material = _onMaterial;
    }

    private void SetDisableMaterial(){
        _sphereMeshRenderer.material = _offMaterial;
    }

    private void ToggleMaterial(){
        _isEnabled = !_isEnabled;
        if (_isEnabled)
            SetEnableMaterial();
        else
            SetDisableMaterial();
    }

    public void PressButton(){
        Debug.Log("[Debug] Button Interact!");
        ToggleMaterial();
    }

    public void Interact(){
        PressButton();
    }

    public string GetInteractText(){
        return interactText;
    }

    public Transform GetTransform() {
        return transform;
    }
}

