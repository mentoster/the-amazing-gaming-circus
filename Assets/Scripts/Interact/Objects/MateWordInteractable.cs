using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MateWordInteractable : MonoBehaviour, IInteractable
{
    [SerializeField] private string interactText;
    private void DestroyMateWord(){
        Destroy(gameObject);
    }

    public void Interact(){
        Debug.Log("[Debug] MateWord Interact!");
        DestroyMateWord();
    }

    public string GetInteractText(){
        return interactText;
    }

    public Transform GetTransform(){
        return transform;
    }
}
