using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.Interactions;

public class PlayerInteract : MonoBehaviour
{
    [SerializeField] private float interactRange = 2f;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            PressInteraction();
        }
    }
    public void PressInteraction()
    {
        IInteractable interactable = GetInteractableObject();
        if (interactable != null)
        {
            interactable.Interact();
        }
    }
    public IInteractable GetInteractableObject()
    {
        List<IInteractable> interactableList = new List<IInteractable>();
        Collider[] colliderArray = Physics.OverlapSphere(transform.position, interactRange);
        foreach (Collider collider in colliderArray)
        {
            if (collider.TryGetComponent(out IInteractable interactable))
            {
                interactableList.Add(interactable);
            }
        }
        IInteractable closestInteractableObject = null;
        foreach (IInteractable interactable in interactableList)
        {
            if (closestInteractableObject == null)
            {
                closestInteractableObject = interactable;
            }
            else
            {
                if (Vector3.Distance(transform.position, interactable.GetTransform().transform.position) <
                    Vector3.Distance(transform.position, closestInteractableObject.GetTransform().transform.position))
                {
                    closestInteractableObject = interactable;
                }
            }
        }

        return closestInteractableObject;
    }
}
