using UnityEngine;
using TMPro;

public class PlayerInteractUI : MonoBehaviour
{
    [SerializeField] private GameObject container;
    [SerializeField] private PlayerInteract playerInteract;
    [SerializeField] private TMP_Text interactText;
    private void Update(){
        if (playerInteract.GetInteractableObject() != null )
            Show(playerInteract.GetInteractableObject());
        else
            Hide();
    }

    private void Show(IInteractable interactable) {
        container.SetActive(true);
        interactText.text = interactable.GetInteractText();
    }

    private void Hide() {
        container.SetActive(false);
    }
}
