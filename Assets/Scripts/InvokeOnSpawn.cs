using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InvokeOnSpawn : MonoBehaviour
{
    public UnityEvent onSpawn; 
    void Start()
    {
        onSpawn.Invoke();
    }
}
