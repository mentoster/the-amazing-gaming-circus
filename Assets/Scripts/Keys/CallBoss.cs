using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CallBoss : MonoBehaviour
{
    private KeyController _keysTaker;

    public UnityEvent called;

    [SerializeField] private GameObject _bossFight;
    private void Start()
    {
        _keysTaker = FindObjectOfType<KeyController>();
    }
    public void CallBossNow()
    {
        if (_keysTaker.CanOpenDoor())
        {
            Debug.Log("CallBossNow");
            called.Invoke();
            _bossFight.SetActive(true);
        }
    }
}
