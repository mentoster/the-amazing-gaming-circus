using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyController : MonoBehaviour
{
    private bool _canOpenDoor = false;
    private bool _haveFirstKey = false;
    private bool _haveSecondKey = false;

    public void TakeFirstKey()
    {
        _haveFirstKey = true;
        Debug.Log("TakeFirstKey");
        KeysCheck();
    }

    public void TakeSecondKey()
    {
        _haveSecondKey = true;
        Debug.Log("TakeSecondKey");
        KeysCheck();
    }

    private void KeysCheck()
    {
        if (_haveFirstKey && _haveSecondKey)
        {
            _canOpenDoor = true;
        }
    }
    public bool CanOpenDoor()
    {
        return _canOpenDoor;
    }
}
