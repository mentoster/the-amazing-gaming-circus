using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeysTaker : MonoBehaviour
{
    private KeyController _keyController;
    [SerializeField] private bool _isFirstKey = false;

    public UnityEvent firstKeyTaken;
    public UnityEvent secondKeyTaken;

    private void Start()
    {
        _keyController = FindObjectOfType<KeyController>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (_isFirstKey)
            {
                _keyController.TakeFirstKey();
            }
            else
            {
                _keyController.TakeSecondKey();
            }

            Destroy(gameObject);
        }
    }
}
