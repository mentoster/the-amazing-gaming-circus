using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    [SerializeField] private Transform startPoint;
    private LineRenderer _lineRenderer;
    void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.positionCount = 2;
        _lineRenderer.SetPosition(0, transform.position);
    }
    void Update()
    {
        Physics.Raycast(startPoint.position, transform.forward,out RaycastHit hit);
        _lineRenderer.SetPosition(1, hit.point);
    }
}
