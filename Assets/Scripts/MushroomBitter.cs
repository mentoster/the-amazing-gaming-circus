using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;
using UnityEngine.Events;

public class MushroomBitter : MonoBehaviour
{
    [SerializeField] private GameObject mushroom;
    [SerializeField] public GameObject mushroomDamaged;
    private FMOD.Studio.EventInstance _screamInstance;
    public EventReference eventReference;
    BannerSpawner _bannerSpawner;
    [SerializeField] private UnityEvent _onMushroomDamaged;

    private bool _isDamaged = false;

    void Start()
    {
        _bannerSpawner = FindAnyObjectByType<BannerSpawner>();
        _bannerSpawner.enabled = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        mushroom.SetActive(false);
        mushroomDamaged.SetActive(true);
        _bannerSpawner.enabled = true;

        if (!_isDamaged)
        {
            _isDamaged = true;
            PlaySound();
            _onMushroomDamaged.Invoke();
        }
    }
    public void PlaySound()
    {
        // Create the instance
        _screamInstance = RuntimeManager.CreateInstance(eventReference);

        // Set the 3D attributes once upon creation, if the position is not supposed to change
        RuntimeManager.AttachInstanceToGameObject(_screamInstance, transform, GetComponent<Rigidbody>());

        // Start the event instance
        _screamInstance.start();
    }
}
