using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class BaseNPC : MonoBehaviour
{
    [Header("Assingables")]
    [HideInInspector] public NavMeshAgent navMeshAgent;
    public LayerMask GroundMask, PlayerMask;

    [SerializeField] protected Transform target;

    [Header("Patroling")]
    public Vector3 walkPoint;
    private bool _isWalkPointSet;
    public float walkPointRange = 10.0f;

    [Header("States")]
    public float sightRange = 10.0f;
    public bool isStopped = false;
    public float pauseDuration = 2.0f;
    public float chaseStoppingDistance = 1.5f;

    protected bool isPlayerInSightRange;
    protected bool stopNabMeshAgent = false;
    private float _rotationSpeed = 10f;
    private Vector3 _startPos =Vector3.zero;

    public bool isFreeze = false;

    protected void Start()
    {
        _startPos = transform.position;
        navMeshAgent = GetComponent<NavMeshAgent>();
        _rotationSpeed = Random.Range(50, 100);
    }

    protected void Update()
    {
        isPlayerInSightRange = Vector3.Distance(transform.position, target.position) <= sightRange;
        if (!isFreeze){
        if (isPlayerInSightRange)
        {
            ChaseTarget();
        }
        else
        {
            Patrolling();
        }
        if (!isPlayerInSightRange)
        {
            transform.Rotate(Vector3.up * _rotationSpeed * Time.deltaTime);
        }
        }
    }

    private void Patrolling()
    {
        if (stopNabMeshAgent)
        {
            return;
        }
        navMeshAgent.stoppingDistance = 0.0f;
        if (!_isWalkPointSet && !isStopped)
        {
            SearchWalkPoint();
        }

        if (_isWalkPointSet)
        {
            navMeshAgent.SetDestination(walkPoint);

            var distanceToWalkPoint = transform.position - walkPoint;

            if (distanceToWalkPoint.magnitude < 1f)
            {
                _isWalkPointSet = false;
                StartCoroutine(PauseBeforeNextMove());
            }
        }
    }

    private void SearchWalkPoint()
    {
        if (stopNabMeshAgent)
        {
            return;
        }
        var randomZ = Random.Range(-walkPointRange, walkPointRange);
        var randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(_startPos.x + randomX, transform.position.y, _startPos.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, GroundMask))
        {
            _isWalkPointSet = true;
        }
    }

    private IEnumerator PauseBeforeNextMove()
    {
        isStopped = true;
        yield return new WaitForSeconds(pauseDuration);
        isStopped = false;
    }

    private void ChaseTarget()
    {
        if (stopNabMeshAgent)
        {
            return;
        }
        // calculate, can reach or no target with path
        if (!navMeshAgent.CalculatePath(target.position, navMeshAgent.path))
        {
            return;
        }
        navMeshAgent.stoppingDistance = chaseStoppingDistance;
        navMeshAgent.SetDestination(target.position);
    }
}
