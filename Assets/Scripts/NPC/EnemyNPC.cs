using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class EnemyNPC : BaseNPC
{
    [Header("Attacking")]
    public float attackRange = 2.0f; // Range within which the enemy can attack.
    public float attackCooldown = 1.5f; // Time between attacks.
    private bool _isInAttackRange;
    private bool _canAttack = true;
    [SerializeField]
    private float _timeToAttack = 0.5f;

    public UnityEvent onAttack;

    protected new void Start()
    {
        base.Start(); // Call the start method of BaseNPC.
        target = FindFirstObjectByType<ThirdPersonController>().transform;
    }

    protected new void Update()
    {
        // Check if the player is within attack range.
        _isInAttackRange = Vector3.Distance(transform.position, target.transform.position) <= attackRange;
        if (_isInAttackRange)
        {
            Debug.Log("Player is in attack range.");
            Attack();
            return;
        }
        base.Update(); // Call the update method of BaseNPC.


    }

    private void Attack()
    {
        if (!_canAttack)
        {
            return;
        }
        navMeshAgent.enabled = false;
        stopNabMeshAgent = true;
        _canAttack = false;
        transform.DORotate(new Vector3(0, 360, 0), 0.5f, RotateMode.FastBeyond360)
            .SetEase(Ease.Linear)
            .OnComplete(OnAttackComplete); // Optional: do something after rotation completes
        onAttack.Invoke();
    }
    private void OnAttackComplete()
    {
        _canAttack = true;
        navMeshAgent.enabled = true;
        stopNabMeshAgent = false;
    }
}
