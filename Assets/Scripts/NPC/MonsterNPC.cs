using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class MonsterNPC : BaseNPC
{

    protected new void Start()
    {
        base.Start(); // Call the start method of BaseNPC.
        this.target = GameObject.FindWithTag("Player").transform;
    }

    protected new void Update()
    {
        base.Update(); // Call the update method of BaseNPC.
    }
}
