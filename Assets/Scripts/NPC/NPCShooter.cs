using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class NPCShooter : BaseNPC
{
    [Header("Attacking")]
    public Object projectile;
    public Transform shootOrigin;
    public float shootForce = 1f;
    public float attackRange = 2.0f; // Range within which the enemy can attack.
    public float attackCooldown = 1.5f; // Time between attacks.
    public float range = 5.0f; // Range within which the enemy should go on to target.
    private bool _isInAttackRange;

    private float _attackTimeout = 0;

    public UnityEvent onAttack;

    protected new void Start()
    {
        base.Start(); // Call the start method of BaseNPC.
        this.target = GameObject.FindWithTag("Player").transform;
    }

    protected new void Update()
    {
        base.Update(); // Call the update method of BaseNPC.

        // Check if the player is within attack range.
        _isInAttackRange = Physics.CheckSphere(transform.position, attackRange, PlayerMask);
        _attackTimeout -= _attackTimeout > 0 ? Time.deltaTime : 0;
        if (_isInAttackRange && _attackTimeout <= 0)
        {
            Attack();
        }
    }

    private void Attack()
    {
        if (Physics.CheckSphere(transform.position, range, PlayerMask))
        {
            navMeshAgent.enabled = false;
            stopNabMeshAgent = true;
        }
        _attackTimeout = attackCooldown;

        onAttack.Invoke();
        ShootTarget();
    }
    private void OnAttackComplete()
    {
        navMeshAgent.enabled = true;
        stopNabMeshAgent = false;
    }

    private void ShootTarget()
    {
        Vector3 direction = (target.position - shootOrigin.position).normalized;

        GameObject newProjectile = Instantiate(projectile, shootOrigin.position, Random.rotation) as GameObject;
        newProjectile.tag = gameObject.tag;

        newProjectile.GetComponent<Rigidbody>().AddForce(direction * shootForce, ForceMode.Impulse);

        transform.rotation = Quaternion.AngleAxis(Vector3.SignedAngle(Vector3.forward, direction, Vector3.up), Vector3.up);
    }
}
