using UnityEngine;
using DG.Tweening;

public class InfiniteRotator : MonoBehaviour
{
    [SerializeField] private Vector3 _rotationAngles = new Vector3(0f, 360f, 0f); // Rotation angles per axis
    [SerializeField] private float _duration = 2f; // Duration for one complete rotation
    [SerializeField] private Ease _easeType = Ease.Linear; // Type of easing, Linear for constant speed

    private void Start()
    {
        // Start the infinite rotation
        StartInfiniteRotation();
    }

    private void StartInfiniteRotation()
    {
        // Create a rotation tween and set it to loop indefinitely
        transform.DORotate(_rotationAngles, _duration, RotateMode.FastBeyond360)
            .SetEase(_easeType)
            .SetLoops(-1, LoopType.Restart); // -1 loops means infinite looping
    }
}
