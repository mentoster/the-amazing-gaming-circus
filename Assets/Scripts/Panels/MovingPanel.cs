using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine;

public class MovingPanel : MonoBehaviour
{
    [SerializeField] private Transform _pointA;
    [SerializeField] private Transform _pointB;
    [SerializeField] private float _moveDuration = 3.0f;
    private Transform _currentTarget;
    private Tween _moveTween;

    private void Start()
    {
        if (_pointA == null || _pointB == null)
        {
            Debug.LogError("Point A and Point B must be assigned");
            enabled = false;
            return;
        }

        // Start moving towards Point B initially
        MoveToNextPoint(_pointB.position);
    }

    private void MoveToNextPoint(Vector3 targetPosition)
    {   
        // Create a tween for the panel to move to the target position and back
        _moveTween = transform.DOMove(targetPosition, _moveDuration)
                              .SetEase(Ease.InOutQuad)
                              .OnComplete(() =>
                                  // Switch target once the current target is reached
                                  MoveToNextPoint(transform.position == _pointA.position ? _pointB.position : _pointA.position));

    }


    private void OnDestroy()
    {
        // It's important to kill the tween if the GameObject is destroyed
        // to prevent any callbacks to objects that no longer exist
        _moveTween?.Kill();
    }
}
