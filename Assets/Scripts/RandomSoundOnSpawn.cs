using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSoundOnSpawn : MonoBehaviour
{
    public float randomPitchDeviationRange = 0.5f;
    public List<AudioClip> clips = new List<AudioClip>();

    private AudioSource _as;

    public void Start(){
        _as = GetComponent<AudioSource>();
        _as.pitch = Random.Range(_as.pitch - randomPitchDeviationRange, _as.pitch + randomPitchDeviationRange);
        _as.clip =clips[Random.Range(0, clips.Count)]; 
        _as.Play();
    } 
}
