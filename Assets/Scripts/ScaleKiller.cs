using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class ScaleKiller : MonoBehaviour
{
    public Transform killObject;
    public string killerTag = "Player"; 

    public float killingDuration = 0.5f;

    public UnityEvent onKillStart;
    public UnityEvent onKillComplete;

    public void OnTriggerEnter(Collider other){
        if (other.gameObject.tag != killerTag )
            return;
            
        killObject.DOScaleY(0,1f)
            .SetEase(Ease.Linear)
            .OnComplete(()=>onKillComplete.Invoke()); // Optional: do something after rotation completes
    }
}
