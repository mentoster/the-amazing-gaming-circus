using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<Object> spawnableObjects = new List<Object>();

    public int maxObjects = 5;
    public float radius = 5f;
    public float spawnCoolDown = 5f;

    public bool spawnAllOnStart = true;

    private float _coolDownTimeout = 0f;

    private List<GameObject> _spawned = new List<GameObject>();

    void Start()
    {
        if (spawnAllOnStart)
        {
            for (int i = 0; i < maxObjects; i++)
            {
                SpawnOnce();
            }
        }

    }

    void Update()
    {
        _coolDownTimeout -= _coolDownTimeout>0?Time.deltaTime:0;
        if (_coolDownTimeout <= 0){
            for (int i = 0; i < _spawned.Count; i++)
                if (_spawned[i] == null)
                    {
                        _spawned.RemoveAt(i);
                        break;
                    }
            if (_spawned.Count < maxObjects)
                SpawnOnce();

            _coolDownTimeout = spawnCoolDown;
        }
    }

    private void SpawnOnce()
    {
        if (spawnableObjects.Count == 0)
            return;
        Vector3 randomPosition = transform.position + Random.insideUnitSphere * radius;
        randomPosition.y = transform.position.y;
        _spawned.Add(Instantiate(spawnableObjects[Random.Range(0, spawnableObjects.Count)], randomPosition, Quaternion.identity) as GameObject);

    }

    public void OnDrawGizmosSelected(){
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
