using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class StartTimer : MonoBehaviour
{
    public float lifetime = 10.0f;
    private float timer = 0.0f;

    private bool _timedOut = false;

    public UnityEvent timeOut;

    public void Start(){
        timer = lifetime;
    } 

    public void Update(){
        timer -= Time.deltaTime;
        if (timer <= 0.0f && !_timedOut){
            timeOut.Invoke();
            _timedOut= true;
        }
    } 
}
