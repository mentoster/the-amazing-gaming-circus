using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.UI;

public class MateBar : MonoBehaviour
{
    public float transparentingSpeed = 0.5f;
    private Image _barImage;
    private Image _backGroundImage;

    private void Awake() {
        _barImage = transform.Find("Bar").GetComponent<Image>();
        _backGroundImage = transform.Find("Background").GetComponent<Image>();
        _barImage.color = new Color(_barImage.color.r, _barImage.color.g, _barImage.color.b, 0);
        _backGroundImage.color = new Color(_barImage.color.r, _barImage.color.g, _barImage.color.b, 0);
        _barImage.fillAmount = 0;
    }

    private void Update(){
        if (_barImage.fillAmount<=0){
            
            _barImage.color = new Color(_barImage.color.r, _barImage.color.g, _barImage.color.b,_barImage.color.a - transparentingSpeed * Time.deltaTime);
            
            _backGroundImage.color = new Color(_barImage.color.r, _barImage.color.g, _barImage.color.b,_barImage.color.a - transparentingSpeed * Time.deltaTime);
            
        }
        else{
            _barImage.color = new Color(_barImage.color.r, _barImage.color.g, _barImage.color.b, 1);
            _backGroundImage.color = new Color(_barImage.color.r, _barImage.color.g, _barImage.color.b, 1);
        }

    }
}


public class Mate {
    public const int MATE_MAX = 100;
    private float _mateAmount;
    private float _mateRegenAmount;

    public Mate() {
        _mateAmount = 100;
        _mateRegenAmount = 30f;
    }

    public void Update(){
        _mateAmount += _mateRegenAmount * Time.deltaTime;
        _mateAmount = Mathf.Clamp(_mateAmount, 0f, MATE_MAX);
    }

    public void TrySpendMate(float amount){
        if (_mateAmount >= amount){
            _mateAmount -= amount;
        }
    }

    public float GetMateNormalized(){
        return _mateAmount / MATE_MAX;
    }

}