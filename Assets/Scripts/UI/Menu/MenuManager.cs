using UnityEngine;

public static class MenuManager
{
    public static bool IsInitialized {get; private set; }
    public static GameObject mainMenu, settingsMenu, pauseMenu;

    public static void Init(){
        GameObject canvas = GameObject.Find("Canvas");
        mainMenu = canvas.transform.Find("MainMenu").gameObject;
        settingsMenu = canvas.transform.Find("SettingsMenu").gameObject;

        IsInitialized = true;
    }

    public static void OpenMenu(Menu menu, GameObject callingMenu)
    {
        if (!IsInitialized)
            Init();

        switch (menu)
        {
            case Menu.MAIN_MENU:
                mainMenu.SetActive(true);
                break;
            case Menu.SETTINGS:
                settingsMenu.SetActive(true);
                break;
        }

        callingMenu.SetActive(false);
    }
}
