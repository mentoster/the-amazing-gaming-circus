using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainMenu : MonoBehaviour
{
    public string levelToLoad = "LoadScene";

    public void SettingsButton(){
        MenuManager.OpenMenu(Menu.SETTINGS, gameObject);
    }
    public void PlayButton(){
        SceneManager.LoadScene(levelToLoad, LoadSceneMode.Single);
    }

    public void ExitButton(){

    }
}
