using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    private  GameObject panel;
    private bool isPaused = false;
    void Awake(){
        panel = gameObject.transform.Find("Panel").gameObject;
    }

    void Update(){
        if (Input.GetKeyDown(KeyCode.Escape)){
            panel.SetActive(!panel.activeSelf);
            TogglePause();
        }
    }

    void TogglePause()
    {
        isPaused = !isPaused;

        if (isPaused)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
    

    public void MainMenuButton(){
        
    }

    public void ContinueButton(){
        TogglePause();
        panel.SetActive(false);
    }
}
