using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class VisibleChecker : MonoBehaviour
{
    public UnityEvent onVisible;
    public UnityEvent onInVisible;

    void OnBecameVisible()
    {
        Debug.Log("aboba");
        onVisible.Invoke();
    }

    void OnBecameInvisible()
    {
        Debug.Log("aboba2");
        onInVisible.Invoke();
    }
}
