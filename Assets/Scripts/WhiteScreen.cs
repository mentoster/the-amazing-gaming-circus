using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WhiteScreen : MonoBehaviour
{
    public float speed = 1f;

    public bool isOn = false;

    private Image _image;

    void Start(){
        _image = GetComponent<Image>();
        if (isOn)
        _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 1);
        else
        _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 0);
    }

    void Update(){
        _image.color = isOn?new Color(_image.color.r, _image.color.g, _image.color.b, Mathf.Clamp01(_image.color.a + speed * Time.deltaTime)):new Color(_image.color.r, _image.color.g, _image.color.b, Mathf.Clamp01(_image.color.a - speed * Time.deltaTime));
    }

    public void SetIsOn(bool value){
        isOn = value;
    }

}
