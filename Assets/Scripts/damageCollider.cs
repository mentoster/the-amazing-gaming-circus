using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class damageCollider : MonoBehaviour
{
    public float damage = 1f;
    public UnityEvent damaged;

    private bool _canDamage = true;

    private void OnTriggerEnter(Collider collider)
    {
        Health health = collider.gameObject.GetComponent<Health>();
        if (health==null)
            health = collider.gameObject.GetComponentInParent<Health>();

        if (collider.gameObject.tag == gameObject.tag || !health|| !_canDamage)
        return;
        
        health.ChangeHP(-damage);
        damaged.Invoke();
    }
    
    public void SetCanDamage(bool value){
        _canDamage = value;
    }
}
