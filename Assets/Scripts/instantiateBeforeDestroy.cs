using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class instantiateBeforeDestroy : MonoBehaviour
{
    public Transform spawnPoint; 
    public List<Object> objects = new List<Object>();

    public void Kill(){
        foreach(Object o in objects){
            Instantiate(o, spawnPoint!=null?spawnPoint.position:transform.position, spawnPoint!=null?spawnPoint.rotation:transform.rotation);
        }
        Destroy(gameObject);
    }
}
