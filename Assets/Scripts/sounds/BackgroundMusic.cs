using UnityEngine;
using FMODUnity; // Make sure you have this namespace to access FMOD functions

public class BackgroundMusic : MonoBehaviour
{
    public EventReference eventReference;

    private FMOD.Studio.EventInstance _musicInstance;

    private void OnEnable()
    {
        _musicInstance = FMODUnity.RuntimeManager.CreateInstance(eventReference);
        _musicInstance.start();
    }

    private void OnDestroy()
    {
        _musicInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        _musicInstance.release();
    }

}
