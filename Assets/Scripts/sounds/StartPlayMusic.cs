using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPlayMusic : MonoBehaviour
{
    [SerializeField] BackgroundMusic _backgroundMusic;

    private bool _audioResumed = false;
    void Update()
    {
        // if press any key or mouse, start playing music
        if (Input.anyKeyDown)
        {
            ResumeAudio();
        }
    }
    public void ResumeAudio()
    {
        if (!_audioResumed)
        {
            var result = FMODUnity.RuntimeManager.CoreSystem.mixerSuspend();
            Debug.Log(result);
            result = FMODUnity.RuntimeManager.CoreSystem.mixerResume();
            Debug.Log(result);
            _audioResumed = true;
            _backgroundMusic.enabled = true;
        }
    }
}
